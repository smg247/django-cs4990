from django.db import models
from django.contrib.auth.models import User
import datetime
from django.utils import timezone
# Create your models here.

class BlogPost(models.Model):
	user = models.ForeignKey(User)
	title = models.CharField(max_length=200)
	body = models.TextField()
	tag = models.CharField(max_length=50)
	pub_date = models.DateTimeField('date published')
	active = models.BooleanField()
	
#the proceeding three functions were taken from the polls app and deal with the pub_date and when a post should be listed as recent
	def was_published_recently(self): 
        	return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    	was_published_recently.admin_order_field = 'pub_date'
    	was_published_recently.boolean = True
    	was_published_recently.short_description = 'Published recently?'

	def __unicode__(self):
		return self.question

	def was_published_recently(self):
		now = timezone.now()
    		return now - datetime.timedelta(days=1) <= self.pub_date <  now


	def __unicode__(self):
		return self.title
