from django.contrib import admin
from blog.models import BlogPost

class BlogPostAdmin(admin.ModelAdmin):
	search_fields = ['title', 'tag', 'body', 'pub_date', 'user', 'active']
	list_display= ('title', 'tag', 'body', 'pub_date', 'user', 'active')
admin.site.register(BlogPost, BlogPostAdmin)
