# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views import generic
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from blog.models import BlogPost

def display_blog(request):
	blogpost_list = BlogPost.objects.filter(active=True).order_by('-pub_date')#this reverses the list of blogposts so I can display the most recent one first
	paginator = Paginator(blogpost_list, 3)

	page = request.GET.get('page')
	try:
		blogposts = paginator.page(page)
	except PageNotAnInteger:
		blogposts = paginator.page(1)
	
	except EmptyPage:
		blogposts = paginator.page(paginator.num_pages)

	return render_to_response('blog/blog.html',{'blogposts':blogposts}, context_instance=RequestContext(request))

def display_post(request, post_id):
	blogposts = BlogPost.objects.all()
	post = get_object_or_404(BlogPost, pk=post_id)
	return render_to_response('blog/post.html',{'post':post}, context_instance=RequestContext(request))

def display_tag(request, tag):
	blogposts = BlogPost.objects.filter(tag=tag)
	tag = get_object_or_404(BlogPost, pk=1)
	return render_to_response('blog/tag.html',{'blogposts':blogposts}, context_instance=RequestContext(request))
