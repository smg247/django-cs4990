from django import template
from blog.models import BlogPost
register = template.Library()
@register.inclusion_tag('blog/recent_posts.html')

def recent_posts():
	recent_list = []
	for post in BlogPost.objects.filter(active=True).order_by('-pub_date')[:5]:
		recent_list.append(post)
	return {'recent_list':recent_list}

@register.inclusion_tag('blog/all_tags.html')
def all_tags():
	tag_list = []
	for post in BlogPost.objects.all():
		if post.tag not in tag_list:
			tag_list.append(post.tag)

	return {'tag_list':tag_list}

