from django.db import models
from sorl.thumbnail import ImageField
# Create your models here.

class CaseStudy(models.Model):
	title = models.CharField(max_length=200)
	description = models.TextField()
	picture = models.ImageField(upload_to="images")
	link = models.URLField(null=True, blank=True)

	def __unicode__(self):
		return self.title
