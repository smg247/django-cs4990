# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views import generic
from django.shortcuts import get_object_or_404
from portfolio.models import CaseStudy

def display_portfolio(request):
	casestudies = CaseStudy.objects.all()
	
	return render_to_response('portfolio.html',{'casestudies':casestudies}, context_instance=RequestContext(request))
	
def display_study(request, study_id):
	casestudies = CaseStudy.objects.all()
	study = get_object_or_404(CaseStudy, pk=study_id)
	return render_to_response('study.html',{'study':study}, context_instance=RequestContext(request))
