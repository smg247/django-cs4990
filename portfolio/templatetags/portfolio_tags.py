from django import template
from portfolio.models import CaseStudy
register = template.Library()
@register.inclusion_tag

def portfolio_images():
	img_list = []
	for study in CaseStudy.objects.all():
		img_list.append(study.picture)
	return {'img_list':img_list}
#remember to load this before you use it in the index or whatever
