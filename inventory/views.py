# Create your views here.

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, HttpResponseRedirect, HttpResponse
from inventory.models import Category, Item
from inventory.forms import ItemForm, CategoryForm, ItemAddForm

def home(request):
	fake = 1# I need to give something to the context, what im making here is a home page that goes to all the apps in this project
	return render_to_response('inventory/home.html',{'fake':fake}, context_instance=RequestContext(request))
	


def display_inventory(request):
	categories = Category.objects.all()
	
	return render_to_response('inventory/inventory.html',{'categories':categories}, context_instance=RequestContext(request))

def display_category(request, cat_id):
	categories = Category.objects.all()
	items = Item.objects.all()
	cat = get_object_or_404(Category, pk=cat_id)
	has_categories = False
	has_items = False

	for category in categories:
		#print category.parent
		#print str(cat_id)
		if unicode(category.parent).split(' ')[0] == unicode(cat_id):  #this was a shot in the dark, it only works if you compare unicode to unicode and the split splits the id off of the unicode
			#print 'true'
			has_categories = True
	for item in items:
		#print category.parent
		if unicode(item.parent).split(' ')[0] == unicode(cat_id):
			has_items = True
	return render_to_response('inventory/category.html',{'cat':cat,'categories':categories, 'items':items, 'has_categories':has_categories, 'has_items':has_items,}, context_instance=RequestContext(request))

def item_edit(request, item_id):
	categories = Category.objects.all()
	item = get_object_or_404(Item, id=item_id)
	item_form = ItemForm(request.POST or None, instance=item)

	if item_form.is_valid():
		item_form.save()
		#return render_to_response('inventory/inventory.html',{'categories':categories}, context_instance=RequestContext(request))
	return render_to_response('inventory/edit.html',{'item':item, 'categories':categories, 'item_form':item_form}, context_instance=RequestContext(request))

def category_add(request, cat_id):
	categories = Category.objects.all()
	cat = get_object_or_404(Category, pk=cat_id)
	has_parent = True
	messages = ""
	if request.method == 'POST':
		category_form = CategoryForm(request.POST)
		if category_form.is_valid():
			cd = category_form.cleaned_data
			new_category, created = Category.objects.get_or_create(name = cd.get('name'), defaults = {'description': cd.get('description'), 'parent':cat})
			if not created:
				messages = "This category already exists!"
			else:
				messages = "Category successfully created"
				new_category.save()

	category_form = CategoryForm()
	context = {
		'form': category_form,
		'messages': messages,
		'categories':categories,
		'cat':cat,
		'has_parent':has_parent,
	}
	return render_to_response('inventory/category_add.html', context,context_instance=RequestContext(request) )

def category_add_base(request):
	categories = Category.objects.all()
	has_parent = False
	messages = ""
	if request.method == 'POST':
		category_form = CategoryForm(request.POST)
		if category_form.is_valid():
			cd = category_form.cleaned_data
			new_category, created = Category.objects.get_or_create(name = cd.get('name'), defaults = {'description': cd.get('description'),})
			if not created:
				messages = "This category already exists!"
			else:
				messages = "Category successfully created!"
				new_category.save()
	category_form = CategoryForm()
	context = {
		'form': category_form,
		'messages': messages,
		'categories':categories,
		'has_parent':has_parent,
	}
	return render_to_response('inventory/category_add.html', context,context_instance=RequestContext(request) )


def item_add(request, cat_id):
	categories = Category.objects.all()
	cat = get_object_or_404(Category, pk=cat_id)
	messages = ""
	if request.method == 'POST':
		item_form = ItemAddForm(request.POST)
		if item_form.is_valid():
			cd = item_form.cleaned_data
			new_item, created = Item.objects.get_or_create(name = cd.get('name'), defaults = {'description': cd.get('description'), 'quantity': cd.get('quantity'), 'parent':cat})
			if not created:
				messages = "This item already exists!"
			else:
				messages = "Item successfully created"
				new_item.save()

	item_form = ItemAddForm()
	context = {
		'form': item_form,
		'messages': messages,
		'categories':categories,
		'cat':cat,
	}
	return render_to_response('inventory/item_add.html', context,context_instance=RequestContext(request) )


def item_qty_get(request, item_id):
	item = get_object_or_404(Item, id=item_id)
	
	return HttpResponse(item.quantity)
