from django.contrib import admin
from inventory.models import Category, Item

class CategoryAdmin(admin.ModelAdmin):
	search_fields = ['name', 'description', 'parent']
	list_display= ('name', 'description', 'parent')
admin.site.register(Category, CategoryAdmin)


class ItemAdmin(admin.ModelAdmin):
	search_fields = ['name', 'description', 'quantity']
	list_display= ('name', 'description', 'quantity')
admin.site.register(Item, ItemAdmin)
