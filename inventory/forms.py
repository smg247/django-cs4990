
from django import forms
from django.forms import ModelForm
from inventory.models import Item

class ItemForm(ModelForm):
	class Meta:
		model = Item
		fields = ['name', 'description','quantity']

class CategoryForm(forms.Form):
	name = forms.CharField(max_length=100)
	description = forms.CharField(max_length=500)

class ItemAddForm(forms.Form):
	name = forms.CharField(max_length=100)
	description = forms.CharField(max_length=500)
	quantity = forms.IntegerField()
