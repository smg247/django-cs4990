from django.db import models

# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
	parent = models.ForeignKey("self", null=True, blank=True)#wow that was a pain to figure out, blank=True even though its ant int not a string...
	def __unicode__(self):
		return u'%d %s' % (self.id, self.name)
class Item(models.Model):	 
	name = models.CharField(max_length=100)
	description = models.TextField()
	quantity = models.IntegerField()
	parent = models.ForeignKey("Category")
	def __unicode__(self):
		return u'%d %s' % (self.id, self.name)
